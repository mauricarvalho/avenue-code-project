# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

Clone the repo: git clone https://mauricarvalho@bitbucket.org/mauricarvalho/avenue-code-project.git

Compile and run the code: mvn spring-boot:run

Run tests: mvn test

### Accessing the REST API: ###

a) Get all products excluding relationships (child products, images)
http://localhost:8080/products/summary

b) Get all products including specified relationships (child product and/or images)
http://localhost:8080/products

c) Same as 1 using specific product identity --IDS available 1,2,3,4,5
http://localhost:8080/products/summary/{id} 
e.g:products/summary/1

d) Same as 2 using specific product identity
http://localhost:8080/products/{id} 

e) Get set of child products for specific product
http://localhost:8080/products/childs/{id}

f) Get set of images for specific product
http://localhost:8080/products/images/{id}

### Accessing database H2: ###

* http://localhost:8080/console
* Driver Class: org.h2.Driver
* JDBC URL: jdbc:h2:mem:avenue
* user: sa
* password:

------------------------------------------------------------------------------

### CRUD: ###
Due the extra time I've got to finished the assessment, I created my CRUD application containing the view layer (JSP + Bootstrap). So the creation, update and delete are done through its own GUI instead of using Postman or any other rest client.

## Main View - "Product Catalog" ##
* http://localhost:8080/list_product.html
* All the products are listed in this screen. All the other operations are accessed through this screen
![ScreenHunter_01 May. 16 17.28.jpg](https://bitbucket.org/repo/Bgg5ARb/images/884054473-ScreenHunter_01%20May.%2016%2017.28.jpg)

## Create Product ##
* Accessed through the button "Create Product" from the "Product Catalog"
* OBS: 
* 1) Enable/Disable - Enable/Disable the Parent Product combobox if the user wants to create a product with/without Parent Product.
* 2) The image relationship is not created in this screen.
![ScreenHunter_02 May. 16 17.29.jpg](https://bitbucket.org/repo/Bgg5ARb/images/1203934157-ScreenHunter_02%20May.%2016%2017.29.jpg)

## Create Image ##
* Accessed through the button "Create Image" from the "Product Catalog"
* OBS: Enable/Disable - Enable/Disable the Parent Product combobox if the user wants to create a image with/without Parent Product.
![ScreenHunter_04 May. 16 17.30.jpg](https://bitbucket.org/repo/Bgg5ARb/images/3315126929-ScreenHunter_04%20May.%2016%2017.30.jpg)

## Edit Product ##
* Accessed through the button "Edit" from the "Product Catalog"
* OBS: Any update in this screen will be reflected at the "Product Catalog![ScreenHunter_06 May. 16 17.31.jpg](https://bitbucket.org/repo/Bgg5ARb/images/1151499837-ScreenHunter_06%20May.%2016%2017.31.jpg)
* Product Description modification reflected
![ScreenHunter_07 May. 16 17.31.jpg](https://bitbucket.org/repo/Bgg5ARb/images/3429245809-ScreenHunter_07%20May.%2016%2017.31.jpg)

## Known Issues ##
* I did not implemented any JSP Page ErrorHandler, so an error, like when trying to delete a product with relationships, the application is not handling nicely:
![ScreenHunter_08 May. 16 17.31.jpg](https://bitbucket.org/repo/Bgg5ARb/images/1944952825-ScreenHunter_08%20May.%2016%2017.31.jpg)