package com.avenue.code.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.avenue.code.project.entity.Image;

@Repository
public interface ImageRepository extends CrudRepository<Image, Long>{

	@Modifying(clearAutomatically = true)
	@Query("select i from Image i where i.product = (from Product p where p.id = :id)")
	List<Image> findImagesByProductId(@Param("id") Long id);
	
	@Modifying(clearAutomatically = true)
	@Query("select i from Image i where i.product is null")
	List<Image> findImagesWithoutParent();

}
