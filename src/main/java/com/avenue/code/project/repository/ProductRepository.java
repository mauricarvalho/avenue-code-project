package com.avenue.code.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.avenue.code.project.Model.ProductModel;
import com.avenue.code.project.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long>{

	@Modifying(clearAutomatically = true)
	@Query("select new com.avenue.code.project.Model.ProductModel(p.id,p.name,p.description) from Product p")
	List<ProductModel> findSummarizedProducts();

	@Modifying(clearAutomatically = true)
	@Query("select new com.avenue.code.project.Model.ProductModel(p.id,p.name,p.description) from Product p where p.id = :id")
	ProductModel findSummarizedProduct(@Param("id") Long id);

	@Modifying(clearAutomatically = true)
	@Query("select p.childs from Product p where p.id = :id")
	List<Product> findChildsByProductId(@Param("id") Long id);

	@Modifying(clearAutomatically = true)
	@Query("select p from Product p where p.id != :id")
	List<Product>  findAnotherProducts(@Param("id") Long id);

}