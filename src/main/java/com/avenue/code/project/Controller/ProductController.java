package com.avenue.code.project.Controller;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.avenue.code.project.Model.ProductModel;
import com.avenue.code.project.Service.ImageService;
import com.avenue.code.project.Service.ProductService;
import com.avenue.code.project.entity.Product;

@Controller
public class ProductController {

	@Autowired
	ProductService productService;

	@Autowired
	ImageService imageService;

	// 1
	@RequestMapping(method = RequestMethod.GET, value = "products/summary", produces = MediaType.APPLICATION_JSON)
	public @ResponseBody List<ProductModel> getSummarizedProducts() {
		return productService.getSummarizedProducts();
	}

	// 2
	@RequestMapping(method = RequestMethod.GET, value = "products", produces = MediaType.APPLICATION_JSON)
	public @ResponseBody List<Product> getDetailedProducts() {
		return productService.getDetailedProducts();
	}

	// 3
	@RequestMapping(method = RequestMethod.GET, value = "products/summary/{id}", produces = MediaType.APPLICATION_JSON)
	public @ResponseBody ProductModel getSummarizedProduct(@PathVariable("id") Long id) {
		return productService.getSummarizedProduct(id);
	}

	// 4
	@RequestMapping(method = RequestMethod.GET, value = "products/{id}", produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Product getDetailedProduct(@PathVariable("id") Long id) {
		return productService.getProduct(id);
	}

	// 5
	@RequestMapping(method = RequestMethod.GET, value = "products/childs/{id}", produces = MediaType.APPLICATION_JSON)
	public @ResponseBody List<Product> getChildsByProductId(@PathVariable("id") Long id) {
		return productService.getChildsByProductId(id);
	}

	@RequestMapping("/list_product.html")
	public ModelAndView listAllProducts() {
		ModelMap model = new ModelMap();
		model.addAttribute("products", productService.getDetailedProducts());
		return new ModelAndView("list_product", model);
	}

	/**
	 * CRUD Methods
	 * 
	 * @param id
	 * @return
	 */

	@RequestMapping("/create_product")
	public ModelAndView listProducts() {
		ModelMap model = new ModelMap();
		// model.addAttribute("images",
		// imageService.getAllImagesWithoutProduct());
		model.addAttribute("products", productService.getDetailedProducts());
		return new ModelAndView("create_product", model);
	}

	@RequestMapping("/update_product")
	public ModelAndView listProduct(Product product) {
		ModelMap model = new ModelMap();
		model.addAttribute("products", productService.getAnotherProducts(product.getId()));
		model.addAttribute("product", productService.getProduct(product.getId()));
		return new ModelAndView("update_product", model);
	}

	@RequestMapping("/delete_product")
	public ModelAndView deleteProduct(Product product) {
		try {
			productService.deleteProduct((product.getId()));
			ModelMap model = new ModelMap();
			model.addAttribute("products", productService.getDetailedProducts());
			return new ModelAndView(new RedirectView("/list_product.html", true), model);
		} catch (Exception e) {
			ModelMap model = new ModelMap();
			model.addAttribute("message", "Error trying to delete the information. Plase, check constraint violation");
			return new ModelAndView(new RedirectView("/error", true), model);
		}

	}

	@RequestMapping(value = "products", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED)
	public @ResponseBody ModelAndView createProduct(Product product) {
		productService.addProduct(product);
		ModelMap model = new ModelMap();
		model.addAttribute("products", productService.getDetailedProducts());
		return new ModelAndView(new RedirectView("/list_product.html", true), model);
	}

	@RequestMapping(value = "products", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_FORM_URLENCODED)
	public @ResponseBody ModelAndView updateProduct(Product product) {
		productService.updateProduct(product.getId(), product);
		ModelMap model = new ModelMap();
		model.addAttribute("products", productService.getDetailedProducts());
		return new ModelAndView(new RedirectView("/list_product.html", true), model);
	}

}