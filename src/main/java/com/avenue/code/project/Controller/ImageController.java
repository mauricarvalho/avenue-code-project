package com.avenue.code.project.Controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.avenue.code.project.Service.ImageService;
import com.avenue.code.project.Service.ProductService;
import com.avenue.code.project.entity.Image;

@RestController
public class ImageController {

	@Autowired
	ImageService imageService;

	@Autowired
	ProductService productService;

	// 6
	@RequestMapping("products/images/{id}")
	public List<Image> getImagesByProductId(@PathVariable("id") Long id) {
		return imageService.getImagesByProductId(id);
	}

	/**
	 * CRUD Methods
	 * 
	 * @param id
	 * @return
	 */

	@RequestMapping("/create_image")
	public ModelAndView listImages() {
		ModelMap model = new ModelMap();
		model.addAttribute("products", productService.getDetailedProducts());
		return new ModelAndView("create_image", model);
	}

	@RequestMapping(value = "images", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED)
	public @ResponseBody ModelAndView createImage(Image image, HttpServletResponse response) {
		imageService.addImage(image);
		ModelMap model = new ModelMap();
		model.addAttribute("products", productService.getDetailedProducts());
		return new ModelAndView(new RedirectView("/list_product.html", true), model);
	}

}
