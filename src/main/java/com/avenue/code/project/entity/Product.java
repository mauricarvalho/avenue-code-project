package com.avenue.code.project.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;

	private String description;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "parent_product_id")
	@JsonBackReference
	private Product parentProduct;

	@OneToMany(mappedBy = "parentProduct", fetch = FetchType.EAGER)
	private Set<Product> childs;

	@OneToMany(mappedBy = "product", targetEntity = Image.class, fetch = FetchType.EAGER)
	private Set<Image> images;

	public Product() {
	}

	public Product(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public Product(String name, String description, Product parentProduct) {
		this.name = name;
		this.description = description;
		this.parentProduct = parentProduct;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Image> getImages() {
		return images;
	}

	public void setImages(Set<Image> images) {
		this.images = images;
	}

	public Product getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(Product parentProduct) {
		this.parentProduct = parentProduct;
	}

	public Set<Product> getChilds() {
		return childs;
	}

	public void setChilds(Set<Product> childs) {
		this.childs = childs;
	}

}