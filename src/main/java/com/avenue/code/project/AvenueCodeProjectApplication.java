package com.avenue.code.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class AvenueCodeProjectApplication extends SpringBootServletInitializer {
	
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AvenueCodeProjectApplication.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(AvenueCodeProjectApplication.class, args);
	}
}
