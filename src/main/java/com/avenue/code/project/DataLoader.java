package com.avenue.code.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.avenue.code.project.entity.Image;
import com.avenue.code.project.entity.Product;
import com.avenue.code.project.repository.ImageRepository;
import com.avenue.code.project.repository.ProductRepository;

@Component
public class DataLoader implements ApplicationRunner {

	private ProductRepository productRepository;

	private ImageRepository imageRepository;

	@Autowired
	public DataLoader(ProductRepository productRepository, ImageRepository imageRepository) {
		this.productRepository = productRepository;
		this.imageRepository = imageRepository;
	}

	public void run(ApplicationArguments args) {
		
		
		Product product1 = new Product("product1", "product1");
		Product product1_1 = new Product("product1_1", "product1_1", product1);
		
		Product product2 = new Product("product2", "product2");
		Product product2_1 = new Product("product2_1", "product2_1", product2);
		Product product2_2 = new Product("product2_2", "product2_2", product2);
		Product product2_1_1 = new Product("product2_1_1", "product2_1_1", product2_1);
		
		
		Image image2_1 = new Image("image2_1", "image2_1", product2);		
		Image image2_2 = new Image("image2_2", "image2_2", product2);
		Image image2_3 = new Image("image2_3", "image2_3", product2);
		Image image2_4 = new Image("image2_4", "image2_4", product2);
		Image image2_5 = new Image("image2_5", "image2_5", product2);
		
		Image image1_1 = new Image("image1_1", "image1_1", product1);		
		Image image1_2 = new Image("image1_2", "image1_2", product1);		
		
		productRepository.save(product1);
		productRepository.save(product1_1);	
		imageRepository.save(image1_1);
		imageRepository.save(image1_2);
	
		
		productRepository.save(product2);
		productRepository.save(product2_1);	
		productRepository.save(product2_2);	
		productRepository.save(product2_1_1);	
		imageRepository.save(image2_1);
		imageRepository.save(image2_2);
		imageRepository.save(image2_3);
		imageRepository.save(image2_4);
		imageRepository.save(image2_5);
			
	}
}
