package com.avenue.code.project.Service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenue.code.project.entity.Image;
import com.avenue.code.project.repository.ImageRepository;

@Service
public class ImageService {

	@Autowired
	ImageRepository imageRepository;

	public List<Image> getAllImages() {
		List<Image> images = new ArrayList<Image>();
		imageRepository.findAll().forEach(images::add);
		return images;
	}
	
	public Image getImage(Long id) {
		return imageRepository.findOne(id);
	}

	public void addImage(Image timage) {
		imageRepository.save(timage);
	}

	public void updateImage(String id, Image timage) {
		imageRepository.save(timage);
	}

	public void deleteImage(Long id) {
		imageRepository.delete(id);
	}

	public List<Image> getImagesByProductId(Long id) {
		return imageRepository.findImagesByProductId(id);
	}
	
	/**CRUD Methods
	 * 
	 * @param id
	 * @return
	 */
	public List<Image> getAllImagesWithoutProduct() {
		List<Image> images = new ArrayList<Image>();
		imageRepository.findImagesWithoutParent().forEach(images::add);
		return images;
	}
}