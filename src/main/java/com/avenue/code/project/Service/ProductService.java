package com.avenue.code.project.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenue.code.project.Model.ProductModel;
import com.avenue.code.project.entity.Product;
import com.avenue.code.project.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;

	public List<Product> getDetailedProducts() {
		List<Product> products = new ArrayList<Product>();
		productRepository.findAll().forEach(products::add);
		return products;
	}

	public Product getProduct(Long id) {
		return productRepository.findOne(id);
	}

	public void addProduct(Product tproduct) {
		productRepository.save(tproduct);
	}

	public void updateProduct(Long id, Product tproduct) {
		productRepository.save(tproduct);
	}

	public void deleteProduct(Long id) {
		productRepository.delete(id);
	}

	public List<ProductModel> getSummarizedProducts() {
		List<ProductModel> products = new ArrayList<ProductModel>();
		productRepository.findSummarizedProducts().forEach(products::add);
		return products;
	}

	public ProductModel getSummarizedProduct(Long id) {
		return productRepository.findSummarizedProduct(id);
	}

	public List<Product> getChildsByProductId(Long id) {
		return productRepository.findChildsByProductId(id);
	}
	
	/**CRUD Methods
	 * 
	 * @param id
	 * @return
	 */
	public List<Product> getAnotherProducts(Long id) {
		List<Product> products = new ArrayList<Product>();
		productRepository.findAnotherProducts(id).forEach(products::add);
		return products;
	}


}