<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Avenue Code</title>
 
<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
<body>
    <jsp:include page="barra_superior_criar_conta.jspf" />
   <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
   <div class="container">
      <div class="navbar-header">
       <a class="navbar-brand" href="">Avenue Code Test</a>
     </div>
   </div>
</div>
    <div class="container" style="padding-top: 80px;">
        <div class="row">
            <div class="container">
                <div class="col-md-8">
                    <h1>Edit Product</h1>
                    <br />
                    <form:form class="form-horizontal" method="put"
                        action="products" role="form" modelAttribute="product">
                        <div class="form-group">
                        <div class="col-sm-8">
                                <label>ID:</label>
                                <input type="text" class="form-control" id="id" name="id"
                                   value="${product.id}" readonly="readonly"/>
                         </div>
                         </div>
                        <div class="form-group">
                            <div class="col-sm-8">
                                 <label>Description:</label>
                                <input type="text" class="form-control" id="description" name="description"
                                   value="${product.description}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8">                            
                            <label>Name:</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    value="${product.name}"/>
                            </div>
                        </div>
                        <div class="form-group">
                        <div class="col-sm-8">
                            <label>Parent Product:</label>
							  <select class="form-control" id="parentProduct" name="parentProduct">
							  <c:forEach var="_product" items="${products}">
								<c:choose>
								  <c:when test="${product.parentProduct.id eq _product.id}">
								    <option selected value="${_product.id}">${_product.id} - ${_product.description}</option>
								  </c:when>
								  <c:otherwise>
								    <option value="${_product.id}">${_product.id} - ${_product.description}</option>
								  </c:otherwise>
								</c:choose>
							  </c:forEach>
							  </select>
							 <label>Enable/Disable:</label>
                            <input type="checkbox" id="check" onclick="document.getElementById('parentProduct').disabled=this.checked;">
						</div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-10">
                                <button type="submit" class="btn btn-default">Update Product</button>
                            </div>
                        </div>
 
                   </form:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>