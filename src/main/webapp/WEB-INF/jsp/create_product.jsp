<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Avenue Code</title>
 
<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
<body>
    <jsp:include page="barra_superior_criar_conta.jspf" />
   <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
   <div class="container">
      <div class="navbar-header">
       <a class="navbar-brand" href="">Avenue Code Test</a>
     </div>
   </div>
</div>
    <div class="container" style="padding-top: 80px;">
        <div class="row">
            <div class="container">
                <div class="col-md-8">
                    <h1>Create Product</h1>
                    <br />
                    <form:form class="form-horizontal" method="post"
                        action="products" role="form" modelAttribute="product">
                        <div class="form-group">
                            <div class="col-sm-8">
                                <label>Description:</label>
                                <input type="text" class="form-control" id="description" name="description"
                                    placeholder="Type the product description" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8">                            
                            <label>Name:</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    placeholder="Type the product name" />
                            </div>
                        </div>
                        <div class="form-group">
                        <div class="col-sm-8">
                            <label>Parent Product:</label>
							  <select class="form-control" id="parentProduct" name="parentProduct">
							  <c:forEach var="product" items="${products}">
							     <option value="${product.id}">${product.id} - ${product.description}</option>
							  </c:forEach>
							  </select>
							 <label>Enable/Disable:</label>
                            <input type="checkbox" id="check" onclick="document.getElementById('parentProduct').disabled=this.checked;">
						</div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-10">
                                <button type="submit" class="btn btn-default">Create Product</button>
                            </div>
                        </div>
 
                   </form:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>