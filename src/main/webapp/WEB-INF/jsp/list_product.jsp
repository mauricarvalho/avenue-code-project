<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Avenue Code</title>
 
<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
<body>
   <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
   <div class="container">
      <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
       <a class="navbar-brand" href="">Avenue Code Test</a>
     </div>
     <div class="navbar-collapse collapse">
        <div class="row" style="padding-top: 10px;">
           <a href="/create_image" class="btn btn-sm btn-success active navbar-right" role="button">Create Image</a>
           <a href="/create_product" style="margin-right: 5px;" class="btn btn-sm btn-success active navbar-right offset" role="button">Create Product</a>
        </div>
     </div>
   </div>
</div>
 
<div class="container">
  <h2>All Products</h2>
  <table class="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Description</th>
        <th>Name</th>
        <th>Parent ID</th>
        <th>Child Images(ID)</th>
      </tr>
    </thead>
    <tbody>
	<c:forEach var="product" items="${products}">
	    <tr class="clickable-row">
	      <td class="col-md-1">${product.id}</td>
	      <td class="col-md-3">${product.description}</td>
	      <td class="col-md-2">${product.name}</td>
	      <td class="col-md-1">${product.parentProduct.id}</td>
	      <td class="col-md-3">
	      <div>
	      <c:forEach var="image" items="${product.images}" varStatus="stat">
			${image.id}&nbsp;
	      </c:forEach> 
	      </div>
	      </td>
	      <td class="col-md-1"><a href="/update_product?id=${product.id}" style="margin-right: 5px;" class="btn btn-sm btn-success active navbar-right offset" role="button">Edit</a></td>
	      <td class="col-md-1"><a href="/delete_product?id=${product.id}" style="margin-right: 5px;" class="btn btn-sm btn-success active navbar-right offset" role="button">Delete</a></td>
	    </tr>
	  </c:forEach>     
    </tbody>
  </table>
</div>
 
   <div class="container">
 
   </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>