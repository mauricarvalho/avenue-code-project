package com.avenue.code.project;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.avenue.code.project.Controller.ProductController;
import com.avenue.code.project.Model.ProductModel;
import com.avenue.code.project.entity.Image;
import com.avenue.code.project.entity.Product;
import com.avenue.code.project.repository.ImageRepository;
import com.avenue.code.project.repository.ProductRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
@ComponentScan("com.avenue")
public class MainResourceTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private ImageRepository imageRepository;

	@MockBean
	ProductRepository productRepository;

	@Test
	public void testSummarizedProducts() throws Exception {

		List<ProductModel> productModel = Arrays.asList(new ProductModel((long) 1, "product1", "product1"));
		given(this.productRepository.findSummarizedProducts()).willReturn(productModel);

		this.mvc.perform(get("/products/summary").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string("[{\"id\":1,\"name\":\"product1\",\"description\":\"product1\"}]"));
	}

	@Test
	public void testDetailedProducts() throws Exception {

		List<Product> product = Arrays.asList(new Product("product1", "product1"));
		given(this.productRepository.findAll()).willReturn(product);

		this.mvc.perform(get("/products").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string(
						"[{\"id\":null,\"name\":\"product1\",\"description\":\"product1\",\"childs\":null,\"images\":null}]"));
	}

	@Test
	public void testSummarizedProduct() throws Exception {

		ProductModel productModel = new ProductModel((long) 1, "product1", "product1");
		given(this.productRepository.findSummarizedProduct((long) 1)).willReturn(productModel);

		this.mvc.perform(get("/products/summary/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string("{\"id\":1,\"name\":\"product1\",\"description\":\"product1\"}"));
	}

	@Test
	public void testDetailedProduct() throws Exception {

		Product product = new Product("product1", "product1");
		given(this.productRepository.findOne((long) 1)).willReturn(product);

		this.mvc.perform(get("/products/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string(
						"{\"id\":null,\"name\":\"product1\",\"description\":\"product1\",\"childs\":null,\"images\":null}"));
	}

	@Test
	public void testChildsByProductId() throws Exception {

		List<Product> product = Arrays.asList(new Product("product1_1", "product1_1"));
		given(this.productRepository.findChildsByProductId((long) 1)).willReturn(product);

		this.mvc.perform(get("/products/childs/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string(
						"[{\"id\":null,\"name\":\"product1_1\",\"description\":\"product1_1\",\"childs\":null,\"images\":null}]"));
	}

	@Test
	public void testImagesByProductId() throws Exception {

		Product product = new Product("product1", "product1");
		List<Image> images = Arrays.asList(new Image("image1", "image1", product));
		given(this.imageRepository.findImagesByProductId((long) 1)).willReturn(images);

		this.mvc.perform(get("/products/images/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string("[{\"id\":null,\"type\":\"image1\",\"description\":\"image1\"}]"));
	}

}